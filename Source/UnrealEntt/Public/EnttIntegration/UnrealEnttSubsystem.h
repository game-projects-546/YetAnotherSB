// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttWorld.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "entt/entt.hpp"
#include "Containers/Ticker.h"

#include "UnrealEnttSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class UNREALENTT_API UUnrealEnttSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()

private:

	enum class EState : uint8
	{
		Play,
		Pause
	};

protected:

	TUniquePtr<FEnttWorld> World;
	FTickerDelegate OnTickDelegate;
	EState State;
	FDelegateHandle OnTickHandle;
	FDelegateHandle OnPauseEditorDetectorHandle;
	FDelegateHandle OnResumeEditorDetectorHandle;


public:

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;
	virtual void Deinitialize() override;
	FEnttWorld* GetECSWorld() const;

private:

	bool Tick(float DeltaTime) const;
};
