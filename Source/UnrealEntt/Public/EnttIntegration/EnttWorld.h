﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "entt/entt.hpp"

using FRegistry = entt::registry;
using EEntity = entt::entity;

namespace YetAnotherSB
{
	enum class ESystemStatus
	{
		Repeat,
        Break
    };
}

class FEnttData final
{
public:
	entt::registry Registry;

public:

	template <typename... Comp, template<typename...> typename List>
	auto Create(List<Comp...>) -> entt::entity
	{
		const auto Entity = Registry.create();
		(Registry.emplace<Comp>(Entity), ...);
		return Entity;
	}

	template <typename... Comp, typename... Exclude, template<typename...> typename List>
	auto GetView(List<Comp...>, entt::exclude_t<Exclude...> ExcludeList = {}) -> decltype(auto)
	{
		return Registry.view<Comp...>(ExcludeList);
	}
};

class FEnttWorld final
{
private:

	template <auto Function>
	struct BasicFunctionProcess : entt::process<BasicFunctionProcess<Function>, float>
	{
	public:

		BasicFunctionProcess()
		{
		}

		void update(float DeltaTime, void* Data)
		{
			if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == YetAnotherSB::ESystemStatus::Repeat)
				return;
			succeed();
		}
	};

	template <auto Function>
	struct FunctionProcessWithTimer : entt::process<FunctionProcessWithTimer<Function>, float>
	{
	private:
		float MaxTime;
		float Remaining;
	public:

		FunctionProcessWithTimer(float SetTimer)
			: MaxTime(SetTimer), Remaining(SMALL_NUMBER)
		{
		}

		void update(float DeltaTime, void* Data)
		{
			Remaining -= FMath::Min(Remaining, DeltaTime);
			if (FMath::IsNearlyEqual(Remaining, 0))
			{
				if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == YetAnotherSB::ESystemStatus::Repeat)
				{
					Remaining = MaxTime;
					return;
				}
				succeed();
			}
		}
	};

	struct LambdaProcess : entt::process<LambdaProcess, float>
	{
	private:
		TFunction<YetAnotherSB::ESystemStatus(FEnttData&, float)> Function;

	public:

		LambdaProcess(TFunction<YetAnotherSB::ESystemStatus(FEnttData&, float)> SetFunction)
			: Function(SetFunction)
		{
		}

		void update(float DeltaTime, void* Data)
		{
			if (Function(*static_cast<FEnttData*>(Data), DeltaTime) == YetAnotherSB::ESystemStatus::Repeat)
				return;
			succeed();
		}
	};

	entt::scheduler<float> Scheduler;

	FEnttData Data;

public:

	template <auto Function>
	void AddSystem()
	{
		Scheduler.attach<BasicFunctionProcess<Function>>();
	}

	void ExecuteSystem(TFunction<YetAnotherSB::ESystemStatus(FEnttData&)> Fun)
	{
		Fun(Data);
	}

	template <auto Function>
	void AddSystemWithTimer(float Time)
	{
		Scheduler.attach<FunctionProcessWithTimer<Function>>(Time);
	}

	bool Tick(float DeltaTime)
	{
		Scheduler.update(DeltaTime, &Data);
		return true;
	}
};
