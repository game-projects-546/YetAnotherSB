﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "UEWorldSet.generated.h"

UINTERFACE(MinimalAPI)
class UUEWorldSet : public UInterface
{
	GENERATED_BODY()
};

class UNREALENTT_API IUEWorldSet
{
	GENERATED_BODY()
	
public:

	virtual void SetUEWorldSet(UWorld* SetWorld) = 0;
};
