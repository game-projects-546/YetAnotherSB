﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttIntegration/EnttWorld.h"
#include "UObject/Interface.h"
#include "ECSWorldSet.generated.h"

UINTERFACE(MinimalAPI)
class UECSWorldSet : public UInterface
{
	GENERATED_BODY()
};

class UNREALENTT_API IECSWorldSet
{
	GENERATED_BODY()
	
public:

	virtual void SetECS(FEnttWorld* SetWorld) = 0;
};
