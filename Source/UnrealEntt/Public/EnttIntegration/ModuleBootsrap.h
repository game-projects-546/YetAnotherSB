﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttWorld.h"
#include "GameFramework/Actor.h"
#include "Modules/EnttModuleBase.h"
#include "ModuleBootsrap.generated.h"

UCLASS(Abstract)
class UNREALENTT_API AModuleBootsrap : public AActor
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<UEnttModuleBase>> EnttModules;

public:
	AModuleBootsrap();

protected:
	virtual void BeginPlay() override;
	virtual void Bootstrap();
};
