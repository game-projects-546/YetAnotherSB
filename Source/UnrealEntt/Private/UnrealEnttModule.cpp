﻿
#include "UnrealEntt/Public/UnrealEnttModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FUnrealEnttModule, UnrealEntt);

DEFINE_LOG_CATEGORY(UnrealEntt);
 
#define LOCTEXT_NAMESPACE "UnrealEntt"
 
void FUnrealEnttModule::StartupModule()
{
	UE_LOG(UnrealEntt, Warning, TEXT("UnrealEntt module has started!"));
}
 
void FUnrealEnttModule::ShutdownModule()
{
	UE_LOG(UnrealEntt, Warning, TEXT("UnrealEntt module has shut down"));
}
 
#undef LOCTEXT_NAMESPACE