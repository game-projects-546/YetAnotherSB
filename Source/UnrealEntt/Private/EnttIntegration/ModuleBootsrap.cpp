﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "EnttIntegration/ModuleBootsrap.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

AModuleBootsrap::AModuleBootsrap()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AModuleBootsrap::BeginPlay()
{
	Super::BeginPlay();
	Bootstrap();
}

void AModuleBootsrap::Bootstrap()
{	
}
