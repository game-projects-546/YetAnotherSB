// Fill out your copyright notice in the Description page of Project Settings.

#include "EnttIntegration/UnrealEnttSubsystem.h"
#include "GameFramework/WorldSettings.h"

void UUnrealEnttSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	World = MakeUnique<FEnttWorld>();
	OnTickDelegate = FTickerDelegate::CreateUObject(this, &UUnrealEnttSubsystem::Tick);
	OnTickHandle = FTicker::GetCoreTicker().AddTicker(OnTickDelegate);
	
	UE_LOG(LogTemp, Warning, TEXT("UUnrealEnttSubsystem has started!"));
	
	Super::Initialize(Collection);
}

void UUnrealEnttSubsystem::Deinitialize()
{
	FTicker::GetCoreTicker().RemoveTicker(OnTickHandle);
	UE_LOG(LogTemp, Warning, TEXT("UUnrealEnttSubsystem has shut down!"));
	Super::Deinitialize();
}

FEnttWorld* UUnrealEnttSubsystem::GetECSWorld() const
{
	return World.Get();
}

bool UUnrealEnttSubsystem::Tick(float DeltaTime) const
{
	if (GetWorld() && !GetWorld()->IsPaused() && World.IsValid())
		World->Tick(DeltaTime);
	return true;
}
