// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class UnrealEntt : ModuleRules
{
	public UnrealEntt(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;
	
		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"YetAnotherSB",
			"EnttLibrary"
		});
		
		PublicIncludePaths.AddRange(new string[] {"UnrealEntt/Public"});
		PrivateIncludePaths.AddRange(new string[] {"UnrealEntt/Private"});
	}
}
