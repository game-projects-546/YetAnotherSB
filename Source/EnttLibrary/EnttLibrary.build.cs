﻿using UnrealBuildTool;

public class EnttLibrary : ModuleRules
{
	public EnttLibrary(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"YetAnotherSB"
		});
		PublicIncludePaths.AddRange(new string[] {"EnttLibrary/Public"});
		PrivateIncludePaths.AddRange(new string[] {"EnttLibrary/Private"});
	}
}