// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class YetAnotherSBTarget : TargetRules
{
	public YetAnotherSBTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange(new string[]
		{
			"YetAnotherSB",
			"EnttLibrary",
			"UnrealEntt",
			"Game"
		});
	}
}