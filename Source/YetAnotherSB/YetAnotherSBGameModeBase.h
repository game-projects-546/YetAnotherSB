// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "YetAnotherSBGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class YETANOTHERSB_API AYetAnotherSBGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
