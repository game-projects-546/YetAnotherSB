﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Components/InstancedStaticMeshComponent.h"
#include "Containers/Queue.h"
#include "GameFramework/Actor.h"
#include "ISMController.generated.h"

UCLASS()
class GAME_API AISMController : public AActor
{
	GENERATED_BODY()

protected:
	
	TArray<FTransform> Transforms;
	TQueue<int> IndexPool;

	UPROPERTY(EditAnywhere)
	UInstancedStaticMeshComponent* MeshComponent;

public:
	AISMController();

	void Initialize(UStaticMesh* Mesh, UMaterialInterface* Material) const;
	int32 AddInstance(const FTransform& Transform);	
	void RemoveInstance(int32 Index);
	void SetTransform(int32 Index, const FTransform& Transform);
	void UpdateTransforms() const;
};
