﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "ArenaController.generated.h"

class UArenaData;
UCLASS()
class GAME_API AArenaController : public AActor
{
	GENERATED_BODY()

protected:

	UPROPERTY()
	UProceduralMeshComponent* MeshComponent;

public:
	AArenaController();

	void Initialize(UArenaData* Data) const;

private:

	void CreateCube(FVector Center, float EdgeLenght) const;
};
