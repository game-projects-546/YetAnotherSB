﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class AISMController;

namespace YetAnotherSB
{
	struct CTransform
	{
		FTransform Value;


		CTransform() = default;

		explicit CTransform(const FTransform& Value)
			: Value(Value)
		{
		}
	};

	template <class T>
	struct CActorPtr
	{
		T* Value;

		CActorPtr() = default;

		explicit CActorPtr(T* Value)
			: Value(Value)
		{
		}
	};

	struct CIndexInController
	{
		int32 Value;

		CIndexInController() = default;

		explicit CIndexInController(int32 Value)
			: Value(Value)
		{
		}
	};


	struct CVelocity
	{
		float Value;


		CVelocity() = default;

		explicit CVelocity(float Value)
			: Value(Value)
		{
		}
	};

	struct CTarget
	{
		entt::entity Value;

		CTarget()
		{
			Value = entt::null;
		};

		explicit CTarget(entt::entity Value)
			: Value(Value)
		{
		}
	};

	struct CShipTeam
	{
		FName Value;

		CShipTeam() = default;

		explicit CShipTeam(FName Value)
			: Value(Value)
		{
		}

		friend bool operator==(const CShipTeam& Lhs, const CShipTeam& RHS)
		{
			return Lhs.Value == RHS.Value;
		}

		friend bool operator!=(const CShipTeam& Lhs, const CShipTeam& RHS)
		{
			return !(Lhs == RHS);
		}
	};

	struct CHealth
	{
		int32 Value;

		CHealth() = default;

		explicit CHealth(int32 Value)
			: Value(Value)
		{
		}
	};

	struct CWeapon
	{
		float DistanceAttack;
		float Cooldown;
		float CurrentCooldown;

		int32 Damage;
		float ProjectileSpeed;
		AISMController* WeaponActorController;
		float ProjectileScale;
		float BeamMeshLength;

		CWeapon() = default;

		CWeapon(int32 Damage, float DistanceAttack, float Recharge,
		        float ProjectileSpeed, AISMController* WeaponActorController, float ProjectileScale,
		        float BeamMeshLength)
			: DistanceAttack(DistanceAttack),
			  Cooldown(Recharge),
			  CurrentCooldown(Recharge),
			  Damage(Damage),
			  ProjectileSpeed(ProjectileSpeed),
			  WeaponActorController(WeaponActorController),
			  ProjectileScale(ProjectileScale),
			  BeamMeshLength(BeamMeshLength)
		{
		}
	};

	struct CProjectileDamage
	{
		int32 Value;

		CProjectileDamage() = default;

		explicit CProjectileDamage(int32 Damage)
			: Value(Damage)
		{
		}
	};

	struct CArena
	{
		FBox Value;

		CArena() = default;

		explicit CArena(const FBox& Value)
			: Value(Value)
		{
		}
	};

	struct CControllerMap
	{
		TMap<uint32, AISMController*> Value;

		CControllerMap() = default;

		explicit CControllerMap(TMap<uint32, AISMController*> Value)
			: Value(Value)
		{
		}
	};
}
