// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ECS/Components/Components.h"
#include "entt/core/type_traits.hpp"

class AShipController;
class AArenaController;
class AISMController;

namespace YetAnotherSB
{
	using CKilled = entt::tag<entt::hashed_string("Killed")>;
	using CActorDisable = entt::tag<entt::hashed_string("ActorDisable")>;
	using CAISMController = CActorPtr<AISMController>;

	using CMapOfControllersTag = entt::tag<entt::hashed_string("MapOfControllers")>;
	using MapOfControllers = entt::type_list_cat<
		entt::type_list<
			CMapOfControllersTag, CControllerMap
		>::type
	>::type;

	using CShipTag = entt::tag<entt::hashed_string("Ship")>;
	using CShipTargetNotFound = entt::tag<entt::hashed_string("ShipTargetNotFound")>;
	using Ship = entt::type_list_cat<
		entt::type_list<
            CShipTag, CShipTargetNotFound, CTransform, CVelocity, CShipTeam, CIndexInController, CWeapon, CTarget,
            CHealth, CAISMController
        >::type
	>::type;

	using CArenaTag = entt::tag<entt::hashed_string("Arena")>;
	using CArenaActorPtr = CActorPtr<AArenaController>;
	using Arena = entt::type_list_cat<
		entt::type_list<CArenaTag, CArena, CArenaActorPtr>::type
	>::type;

	using CProjectileTag = entt::tag<entt::hashed_string("Projectile")>;
	using Projectile = entt::type_list_cat<
		entt::type_list<
			CProjectileTag, CProjectileDamage, CTransform, CVelocity, CTarget, CAISMController, CIndexInController
		>::type
	>::type;
	using FreeProjectile = entt::type_list_cat<
		entt::type_list<
			CActorDisable, CKilled
		>::type,
		Projectile
	>::type;
}
