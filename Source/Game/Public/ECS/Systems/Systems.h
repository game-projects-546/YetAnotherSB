// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "ECS/Components/Components.h"
#include "ECS/Entities/Entities.h"
#include "EnttIntegration/EnttWorld.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/NumericLimits.h"
#include "Scene/ISMController.h"

namespace YetAnotherSB
{
	FORCEINLINE ESystemStatus SCheckTarget(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CShipTag const, CTarget const>{},
		             entt::exclude_t<CKilled>{}).each(
			[&Data](auto Entity, const auto& Target)
			{
				if (Data.Registry.has<CKilled>(Target.Value))
					Data.Registry.emplace<CShipTargetNotFound>(Entity);
				//if(Data.Registry.valid(Target.Value))
					//Data.Registry.emplace_or_replace<CShipTargetNotFound>(Entity);
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SFindShipTarget(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CShipTag const, CTransform const, CShipTeam const, CShipTargetNotFound>{},
		             entt::exclude_t<CKilled>{})
		    .each([&Data](auto Entity, const auto& Transform, const auto& ShipTeam)
			    {
				    entt::entity TargetEntity = entt::null;
				    float SmallestDistance = TNumericLimits<float>::Max();
				    Data.GetView(entt::type_list<CShipTag const, CTransform const, CShipTeam const>{},
				                 entt::exclude_t<CKilled>{}).each(
					    [&TargetEntity, SmallestDistance, SelfEntity = Entity,
						    SelfPosition = Transform.Value.GetLocation(), &SelfShipTeam = ShipTeam](
					    auto Entity, const auto& Transform, const auto& ShipTeam) mutable
					    {
						    if (SelfShipTeam == ShipTeam)
							    return;

						    const auto NewDistance = FVector::Dist(Transform.Value.GetLocation(), SelfPosition);
						    if (NewDistance < SmallestDistance)
						    {
							    SmallestDistance = NewDistance;
							    TargetEntity = Entity;
						    }
					    });
				    if (TargetEntity == entt::null)
					    return;
				    Data.Registry.replace<CTarget>(Entity, TargetEntity);
				    Data.Registry.remove<CShipTargetNotFound>(Entity);
			    }
		    );
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SMoveShip(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CShipTag const, CVelocity const, CTransform, CTarget const>{},
		             entt::exclude_t<CKilled, CShipTargetNotFound>{})
		    .each([&Data, DeltaTime](auto Entity, const auto& Velocity, auto& Transform, const auto& Target)
		    {
			    const auto& TargetTransform = Data.Registry.get<CTransform>(Target.Value);
			    auto LocalTransform = Transform.Value;
			    const auto Distance = FVector::Dist(TargetTransform.Value.GetLocation(), Transform.Value.GetLocation());
			    auto Direction = (TargetTransform.Value.GetLocation() - Transform.Value.GetLocation())
				    .GetSafeNormal();
			    auto VectorVelocity = Direction * Velocity.Value;
			    if (Distance < Velocity.Value * DeltaTime)
				    return;
			    LocalTransform.SetLocation(LocalTransform.GetLocation() + VectorVelocity * DeltaTime);

			    auto Rotator = FRotationMatrix::MakeFromY(-Direction).Rotator();
			    LocalTransform.SetRotation(Rotator.Quaternion());

			    Transform = CTransform(LocalTransform);
		    });
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SCheckOutOfBounds(FEnttData& Data, float DeltaTime)
	{
		auto View = Data.GetView(entt::type_list<CArenaTag const, CArena const>{});
		if (View.begin() == View.end())
			return ESystemStatus::Repeat;
		auto [Arena] = View.get(*View.begin());
		Data.GetView(entt::type_list<CShipTag const, CTransform const>{}, entt::exclude_t<CKilled>{}).each(
			[&Data, &Arena](auto Entity, const auto& Transform)
			{
				const auto Position = Transform.Value.GetLocation();
				if (Arena.Value.IsInsideOrOn(Position))
					return;
				Data.Registry.emplace<CKilled>(Entity);
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SAttackShip(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CShipTag const, CTransform const, CTarget const, CWeapon>{},
		             entt::exclude_t<CKilled, CShipTargetNotFound>{}).each(
			[&Data, DeltaTime](auto Entity, const auto& Transform, const auto& Target, auto& Weapon)
			{
				auto ProjectileVi = Data.GetView(FreeProjectile{});
				check(ProjectileVi.begin() != ProjectileVi.end());

				Weapon.CurrentCooldown -= std::min(Weapon.CurrentCooldown, DeltaTime);
				if (Weapon.CurrentCooldown > 0)
					return;

				Weapon.CurrentCooldown = Weapon.Cooldown;

				const auto& TargetTransform = Data.Registry.get<CTransform>(Target.Value);
				const auto Distance = FVector::Dist(TargetTransform.Value.GetLocation(), Transform.Value.GetLocation());
				if (Weapon.DistanceAttack < Distance)
					return;

				auto [ProjectileDamage, ProjectileTransform, ProjectileVelocity, ProjectileTarget,
					ProjectileAISMController,
					ProjectileIndexInController] = ProjectileVi.get<
					CProjectileDamage, CTransform, CVelocity, CTarget, CAISMController, CIndexInController>(
					*ProjectileVi.begin());

				ProjectileDamage = CProjectileDamage(Weapon.Damage);
				auto LocalTransform = Transform.Value;
				const auto Scale = FVector(Weapon.ProjectileScale, Weapon.ProjectileScale, Weapon.ProjectileScale);
				LocalTransform.SetScale3D(Scale);
				ProjectileTransform = CTransform(LocalTransform);
				ProjectileVelocity = CVelocity(Weapon.ProjectileSpeed);
				ProjectileTarget = Target;
				ProjectileAISMController = CAISMController(Weapon.WeaponActorController);
				ProjectileIndexInController = CIndexInController(
					Weapon.WeaponActorController->AddInstance(ProjectileTransform.Value));
				Data.Registry.remove<CActorDisable, CKilled>(*ProjectileVi.begin());

				auto CurrentProjectileVi = Data.GetView(FreeProjectile{});
				if (CurrentProjectileVi.begin() == CurrentProjectileVi.end())
					Data.Create(FreeProjectile{});
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SMoveProjectLine(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(
			    entt::type_list<CProjectileTag const, CVelocity const, CTransform, CTarget const, CProjectileDamage
			                    const>{},
			    entt::exclude_t<CKilled, CActorDisable>{})
		    .each([&Data, DeltaTime](auto Entity, const auto& Velocity, auto& Transform, const auto& Target,
		                             const auto& ProjectileDamage)
		    {
			    const auto& TargetTransform = Data.Registry.get<CTransform>(Target.Value);
			    auto LocalTransform = Transform.Value;
			    const auto Distance = FVector::Dist(TargetTransform.Value.GetLocation(), Transform.Value.GetLocation());
			    auto Direction = (TargetTransform.Value.GetLocation() - Transform.Value.GetLocation())
				    .GetSafeNormal();
			    auto VectorVelocity = Direction * Velocity.Value;
			    if (Distance < Velocity.Value * DeltaTime)
			    {
				    check(Data.Registry.has<CHealth>(Target.Value));
				    auto& Health = Data.Registry.get<CHealth>(Target.Value);
				    Health.Value -= std::min(Health.Value, ProjectileDamage.Value);
			    	Data.Registry.emplace<CKilled>(Entity);
				    return;
			    }
			    LocalTransform.SetLocation(LocalTransform.GetLocation() + VectorVelocity * DeltaTime);

			    auto Rotator = FRotationMatrix::MakeFromY(-Direction).Rotator();
			    LocalTransform.SetRotation(Rotator.Quaternion());

			    Transform = CTransform(LocalTransform);
		    });
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SCheckHPShip(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CShipTag const, CHealth const>{},
		             entt::exclude_t<CKilled>{}).each(
			[&Data](auto Entity, const auto& Health)
			{
				if (Health.Value > 0)
					return;

				Data.Registry.emplace<CKilled>(Entity);
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SKill(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CKilled, CIndexInController, CAISMController>{},
		             entt::exclude_t<CActorDisable>{}).each(
			[&Data](auto Entity, const auto& IndexInController, auto& AISMController)
			{
				AISMController.Value->RemoveInstance(IndexInController.Value);
				Data.Registry.emplace<CActorDisable>(Entity);
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SSendTransformToActor(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CTransform const, CIndexInController const, CAISMController>{},
		             entt::exclude_t<CActorDisable>{}).each(
			[](auto Entity, const auto& Transform, const auto& Index, auto& AISMController)
			{
				AISMController.Value->SetTransform(Index.Value, Transform.Value);
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SUpdateController(FEnttData& Data, float DeltaTime)
	{
		Data.GetView(entt::type_list<CMapOfControllersTag, CControllerMap>{}).each(
			[](auto Entity, const auto& ControllerMap)
			{
				for (auto Element : ControllerMap.Value)
					Element.Value->UpdateTransforms();
			}
		);
		return ESystemStatus::Repeat;
	}

	FORCEINLINE ESystemStatus SLog(FEnttData& Data, float DeltaTime)
	{
		/*
		auto Vi = Data.GetView(Ship
			{});
		if (Vi.begin() != Vi.end())
		{
			auto& Transform = Vi.get<CTransform>(*Vi.begin());
			UE_LOG(LogTemp, Warning, TEXT("Entity %s with Position %s"),
			       *FString::FromInt(static_cast<int32>(*Vi.begin())),
			       *Transform.Value.ToString());
			if (Data.Registry.has<CTarget>(*Vi.begin()))
			UE_LOG(LogTemp, Warning, TEXT("Entity has Target"));
		}
		*/
		return ESystemStatus::Repeat;
	}
}
