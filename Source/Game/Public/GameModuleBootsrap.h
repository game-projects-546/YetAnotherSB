// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttIntegration/ModuleBootsrap.h"
#include "GameModuleBootsrap.generated.h"

class UGameData;
/**
 * 
 */
UCLASS()
class GAME_API AGameModuleBootsrap : public AModuleBootsrap
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere)
	UGameData* GameData;


protected:
	virtual void Bootstrap() override;
};
