﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GameDataSet.generated.h"

class UGameData;

UINTERFACE()
class UGameDataSet : public UInterface
{
	GENERATED_BODY()
};

class GAME_API IGameDataSet
{
	GENERATED_BODY()

public:

	virtual void SetGameData(UGameData* SetShipData) = 0;
};
