// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttIntegration/Modules/EnttModuleBase.h"
#include "EnttIntegration/Modules/ModuleInterface/ECSWorldSet.h"
#include "EnttIntegration/Modules/ModuleInterface/UEWorldSet.h"
#include "ModuleInterface/GameDataSet.h"
#include "SceneRegistrationModule.generated.h"

class AArenaController;
class UArenaData;
class UShipData;
/**
 * 
 */
UCLASS()
class GAME_API USceneRegistrationModule : public UEnttModuleBase, public IUEWorldSet, public IECSWorldSet,
                                          public IGameDataSet
{
	GENERATED_BODY()

private:

	FEnttWorld* EnttWorldPtr;

	UPROPERTY()
	UWorld* UEWorld;

	UPROPERTY()
	UGameData* GameData;

public:
	virtual void SetECS(FEnttWorld* SetWorld) override;
	virtual void SetUEWorldSet(UWorld* SetWorld) override;
	virtual void SetGameData(UGameData* SetGameData) override;
	virtual void Initialize() override;
};
