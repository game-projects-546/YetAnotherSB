// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnttIntegration/Modules/EnttModuleBase.h"
#include "EnttIntegration/Modules/ModuleInterface/ECSWorldSet.h"
#include "GameSystemsRegistrationModule.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UGameSystemsRegistrationModule final : public UEnttModuleBase, public IECSWorldSet
{
	GENERATED_BODY()

private:

	FEnttWorld* WorldPtr;

public:
	virtual void SetECS(FEnttWorld* SetWorld) override;
	virtual void Initialize() override;
};
