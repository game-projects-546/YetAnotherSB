// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/StaticMesh.h"

#include "ArenaData.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UArenaData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(EditAnywhere)
	UMaterialInterface* Material = nullptr;
	UPROPERTY(EditAnywhere)
	FVector Center;
	UPROPERTY(EditAnywhere)
	float EdgeLenght;
};
