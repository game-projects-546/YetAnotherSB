// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "ShipData.h"
#include "TeamData.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UTeamData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	UShipData* ShipData;	
	UPROPERTY(EditAnywhere)
	FName TeamName;
	UPROPERTY(EditAnywhere)
	int32 Count;
};
