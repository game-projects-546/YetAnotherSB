// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "WeaponData.h"
#include "Engine/DataAsset.h"
#include "Engine/StaticMesh.h"

#include "ShipData.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UShipData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	UStaticMesh* Mesh = nullptr;
	UPROPERTY(EditAnywhere)
	UMaterialInterface* Material = nullptr;
	UPROPERTY(EditAnywhere)
	UWeaponData* WeaponData;
	UPROPERTY(EditAnywhere)
	float Speed;
	UPROPERTY(EditAnywhere)
	int32 HP;
};
