// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TeamData.h"
#include "Engine/DataAsset.h"
#include "GameData.generated.h"

class UShipData;
class UArenaData;
/**
 * 
 */
UCLASS()
class GAME_API UGameData : public UDataAsset
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(EditAnywhere)
	UTeamData* RedShipTeamData = nullptr;
	UPROPERTY(EditAnywhere)
	UTeamData* BlueShipTeamData = nullptr;
	UPROPERTY(EditAnywhere)
	UArenaData* ArenaData = nullptr;
};
