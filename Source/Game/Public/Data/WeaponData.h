// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Engine/StaticMesh.h"
#include "WeaponData.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UWeaponData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	float DistanceAttack;
	UPROPERTY(EditAnywhere)
	float Cooldown;

	UPROPERTY(EditAnywhere, Category=Projectile)
	UStaticMesh* ProjectileMesh = nullptr;
	UPROPERTY(EditAnywhere, Category=Projectile)
	UMaterialInterface* ProjectileMaterial = nullptr;
	UPROPERTY(EditAnywhere, Category=Projectile)
	float ProjectileSpeed;
	UPROPERTY(EditAnywhere, Category=Projectile)
	int32 Damage;
	UPROPERTY(EditAnywhere, Category=Projectile)
	float ProjectileScale;
	UPROPERTY(EditAnywhere, Category=Projectile)
	float BeamMeshLength;
};
