﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/ArenaController.h"

#include "Data/ArenaData.h"

AArenaController::AArenaController()
{
	PrimaryActorTick.bCanEverTick = false;
	MeshComponent = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ProceduralMeshComponent"));
	SetRootComponent(MeshComponent);
}

void AArenaController::Initialize(UArenaData* Data) const
{
	MeshComponent->CastShadow = false;
	MeshComponent->bCastDynamicShadow = false;
	MeshComponent->bCastStaticShadow = false;
	MeshComponent->SetGenerateOverlapEvents(false);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent->SetCanEverAffectNavigation(false);
	MeshComponent->SetMaterial(0, Data->Material);

	CreateCube(Data->Center, Data->EdgeLenght);
}

void AArenaController::CreateCube(FVector Center, float EdgeLenght) const
{
	TArray<FVector> Vertices;
	TArray<int32> Triangles;	
	auto AddTriangle = [&Triangles](int32 One, int32 Two, int32 Three)
	{
		Triangles.Add(One);
		Triangles.Add(Two);
		Triangles.Add(Three);
	};

	Vertices.Add(FVector(Center.X - EdgeLenght/2, Center.Y - EdgeLenght/2, Center.Z - EdgeLenght/2));
	Vertices.Add(FVector(Center.X - EdgeLenght/2, Center.Y - EdgeLenght/2, Center.Z + EdgeLenght/2));
	Vertices.Add(FVector(Center.X - EdgeLenght/2, Center.Y + EdgeLenght/2, Center.Z - EdgeLenght/2));
	Vertices.Add(FVector(Center.X - EdgeLenght/2, Center.Y + EdgeLenght/2, Center.Z + EdgeLenght/2));
	
	Vertices.Add(FVector(Center.X + EdgeLenght/2, Center.Y - EdgeLenght/2, Center.Z - EdgeLenght/2));
	Vertices.Add(FVector(Center.X + EdgeLenght/2, Center.Y - EdgeLenght/2, Center.Z + EdgeLenght/2)); 
	Vertices.Add(FVector(Center.X + EdgeLenght/2, Center.Y + EdgeLenght/2, Center.Z + EdgeLenght/2));
	Vertices.Add(FVector(Center.X + EdgeLenght/2, Center.Y + EdgeLenght/2, Center.Z - EdgeLenght/2));
 

	AddTriangle(3, 2, 0);
	AddTriangle(0, 1, 3); 

	AddTriangle(4, 1, 0);
	AddTriangle(5, 1, 4); 

	AddTriangle(7, 5, 4);
	AddTriangle(6, 5, 7);
 
	AddTriangle(3, 6, 7);
	AddTriangle(7, 2, 3);

	AddTriangle(5, 3, 1);
	AddTriangle(3, 5, 6);

	AddTriangle(4, 0, 2);
	AddTriangle(2, 7, 4);	

	MeshComponent->CreateMeshSection(0, Vertices, Triangles, TArray<FVector>(), TArray<FVector2D>(),
                                     TArray<FColor>(), TArray<FProcMeshTangent>(), false);
}
