﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Scene/ISMController.h"

AISMController::AISMController()
{
	PrimaryActorTick.bCanEverTick = false;

	MeshComponent = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("MeshComponent"));
	SetRootComponent(MeshComponent);
}

void AISMController::Initialize(UStaticMesh* Mesh, UMaterialInterface* Material) const
{
	MeshComponent->SetStaticMesh(Mesh);
	MeshComponent->SetMaterial(0, Material);
	MeshComponent->CastShadow = false;
	MeshComponent->bCastDynamicShadow = false;
	MeshComponent->bCastStaticShadow = false;
	MeshComponent->bUseDefaultCollision = false;
	MeshComponent->SetGenerateOverlapEvents(false);
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeshComponent->SetCanEverAffectNavigation(false);
	MeshComponent->NumCustomDataFloats = 1;
}

int32 AISMController::AddInstance(const FTransform& Transform)
{
	if(IndexPool.IsEmpty())
	{
		Transforms.Add(Transform);
		return MeshComponent->AddInstance(Transform);
	}

	int32 Index;
	IndexPool.Dequeue(Index);
	MeshComponent->SetCustomDataValue(Index, 0, 0);
	return Index;
}

void AISMController::RemoveInstance(int32 Index)
{
	IndexPool.Enqueue(Index);

	//Hide mesh without removing
	MeshComponent->SetCustomDataValue(Index, 0, 1);
}

void AISMController::SetTransform(int32 Index, const FTransform& Transform)
{
	Transforms[Index] = Transform;
}

void AISMController::UpdateTransforms() const
{
	if(Transforms.Num() > 0)
		MeshComponent->BatchUpdateInstancesTransforms(0, Transforms, true, true);
	
}
