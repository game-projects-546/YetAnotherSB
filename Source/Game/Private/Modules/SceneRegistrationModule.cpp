// Fill out your copyright notice in the Description page of Project Settings.


#include "Modules/SceneRegistrationModule.h"
#include "Data/ArenaData.h"
#include "Data/GameData.h"
#include "Data/ShipData.h"
#include "ECS/Systems/Systems.h"
#include "Scene/ArenaController.h"
#include "Scene/ISMController.h"

namespace YetAnotherSB
{
	FORCEINLINE auto GetOrCreateController(UWorld* UEWorld, UStaticMesh* Mesh, UMaterialInterface* Material,
	                                       CControllerMap& Map) -> decltype(auto)
	{
		const auto Hash = HashCombine(GetTypeHash(Material), GetTypeHash(Mesh));

		const auto Find = Map.Value.Find(Hash);
		if (Find == nullptr)
		{
			const FVector Location = FVector::ZeroVector;
			const FRotator Rotation = FRotator::ZeroRotator;
			const FActorSpawnParameters SpawnInfo;
			const auto Controller = UEWorld->SpawnActor<AISMController>(AISMController::StaticClass(), Location,
			                                                            Rotation,
			                                                            SpawnInfo);
			Controller->Initialize(Mesh, Material);

			Map.Value.Add(Hash, Controller);
		}
		return Hash;
	}

	FORCEINLINE auto AddNewInstance(UWorld* UEWorld, UStaticMesh* Mesh, UMaterialInterface* Material,
	                                const CTransform& Transform, CControllerMap& Map)
	{
		const auto Hash = GetOrCreateController(UEWorld, Mesh, Material, Map);
		auto ControllerPtr = Map.Value[Hash];
		const auto ShipIndexInController = ControllerPtr->AddInstance(Transform.Value);
		return std::make_tuple(ControllerPtr, ShipIndexInController);
	}

	FORCEINLINE AArenaController* CreateArenaController(UWorld* UEWorld, UArenaData* ArenaData)
	{
		const FVector Location = ArenaData->Center;
		const FRotator Rotation = FRotator::ZeroRotator;
		const FActorSpawnParameters SpawnInfo;
		const auto Controller = UEWorld->SpawnActor<AArenaController>(AArenaController::StaticClass(), Location,
		                                                              Rotation,
		                                                              SpawnInfo);
		Controller->Initialize(ArenaData);
		return Controller;
	}

	FORCEINLINE auto CreateECSShip(UTeamData* TeamData, UWorld* UEWorld) -> decltype(auto)
	{
		return [TeamData, UEWorld](FEnttData& Data)
		{
			auto ShipData = TeamData->ShipData;
			const auto WeaponData = ShipData->WeaponData;
			auto ArenaView = Data.GetView(entt::type_list<CArenaTag const, CArena const>{});
			auto [Arena] = ArenaView.get(*ArenaView.begin());
			auto ControllerMapView = Data.GetView(entt::type_list<CMapOfControllersTag, CControllerMap>{});
			auto [ControllerMap] = ControllerMapView.get(*ControllerMapView.begin());

			auto WeaponControllerKey = GetOrCreateController(UEWorld, WeaponData->ProjectileMesh,
			                                                 WeaponData->ProjectileMaterial, ControllerMap);
			auto WeaponController = ControllerMap.Value[WeaponControllerKey];

			for (auto Index = 0; Index < TeamData->Count; ++Index)
			{
				auto Entity = Data.Create(Ship{});
				auto [Transform, Velocity, ShipTeam, Health, IndexInController, AISMController, Weapon] =
					Data.Registry.get<
						CTransform, CVelocity, CShipTeam, CHealth, CIndexInController,
						CAISMController, CWeapon>(Entity);
				Transform = CTransform(FTransform(FMath::RandPointInBox(Arena.Value)));
				Velocity = CVelocity(ShipData->Speed);
				ShipTeam = CShipTeam(TeamData->TeamName);
				Health = CHealth(ShipData->HP);

				auto [Controller, Id] = AddNewInstance(UEWorld, ShipData->Mesh, ShipData->Material, Transform,
				                                       ControllerMap);
				IndexInController = CIndexInController(Id);
				AISMController = CAISMController(Controller);
				Weapon = CWeapon(WeaponData->Damage, WeaponData->DistanceAttack, WeaponData->Cooldown,
				                 WeaponData->ProjectileSpeed, WeaponController, WeaponData->ProjectileScale,
				                 WeaponData->BeamMeshLength);
			}
			return ESystemStatus::Break;
		};
	}

	FORCEINLINE auto CreateECSArena(UArenaData* ArenaData, UWorld* UEWorld) -> decltype(auto)
	{
		return [ArenaData, UEWorld](FEnttData& Data)
		{
			auto Entity = Data.Create(Arena{});
			auto [Arena, ArenaController] = Data.Registry.get<
				CArena, CArenaActorPtr>(Entity);
			Arena = CArena(FBox(
				ArenaData->Center - ArenaData->EdgeLenght / 2, ArenaData->Center + ArenaData->EdgeLenght / 2));
			ArenaController = CArenaActorPtr(CreateArenaController(UEWorld, ArenaData));
			return ESystemStatus::Break;
		};
	}

	FORCEINLINE auto CreateECSProjectile() -> decltype(auto)
	{
		return [](FEnttData& Data)
		{
			Data.Create(FreeProjectile{});
			return ESystemStatus::Break;
		};
	}

	FORCEINLINE auto CreateMapOfControllers() -> decltype(auto)
	{
		return [](FEnttData& Data)
		{
			auto Entity = Data.Create(MapOfControllers{});
			return ESystemStatus::Break;
		};
	}
}

void USceneRegistrationModule::SetECS(FEnttWorld* SetWorld)
{
	EnttWorldPtr = SetWorld;
}

void USceneRegistrationModule::SetUEWorldSet(UWorld* SetWorld)
{
	UEWorld = SetWorld;
}

void USceneRegistrationModule::SetGameData(UGameData* SetGameData)
{
	GameData = SetGameData;
}

void USceneRegistrationModule::Initialize()
{
	check(EnttWorldPtr);
	check(UEWorld);
	check(GameData);
	check(GameData->ArenaData);
	check(GameData->BlueShipTeamData);
	check(GameData->RedShipTeamData);

	EnttWorldPtr->ExecuteSystem(YetAnotherSB::CreateMapOfControllers());
	EnttWorldPtr->ExecuteSystem(YetAnotherSB::CreateECSArena(GameData->ArenaData, UEWorld));
	EnttWorldPtr->ExecuteSystem(YetAnotherSB::CreateECSShip(GameData->RedShipTeamData, UEWorld));
	EnttWorldPtr->ExecuteSystem(YetAnotherSB::CreateECSShip(GameData->BlueShipTeamData, UEWorld));
	EnttWorldPtr->ExecuteSystem(YetAnotherSB::CreateECSProjectile());
}
