// Fill out your copyright notice in the Description page of Project Settings.

#include "Modules/GameSystemsRegistrationModule.h"
#include "ECS/Systems/Systems.h"

void UGameSystemsRegistrationModule::SetECS(FEnttWorld* SetWorld)
{
	WorldPtr = SetWorld;
}

void UGameSystemsRegistrationModule::Initialize()
{
	check(WorldPtr);
	WorldPtr->AddSystem<&YetAnotherSB::SCheckTarget>();
	WorldPtr->AddSystem<&YetAnotherSB::SFindShipTarget>();
	WorldPtr->AddSystem<&YetAnotherSB::SMoveShip>();
	WorldPtr->AddSystem<&YetAnotherSB::SCheckOutOfBounds>();
	WorldPtr->AddSystem<&YetAnotherSB::SAttackShip>();
	WorldPtr->AddSystem<&YetAnotherSB::SMoveProjectLine>();
	WorldPtr->AddSystem<&YetAnotherSB::SCheckHPShip>();	
	WorldPtr->AddSystem<&YetAnotherSB::SSendTransformToActor>();
	WorldPtr->AddSystem<&YetAnotherSB::SUpdateController>();
	WorldPtr->AddSystem<&YetAnotherSB::SKill>();
	
	//WorldPtr->AddSystem<&YetAnotherSB::SLog>();	
}
