﻿
#include "Game/Public/GameModule.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

IMPLEMENT_GAME_MODULE(FGameModule, Game);

DEFINE_LOG_CATEGORY(Game);
 
#define LOCTEXT_NAMESPACE "Game"
 
void FGameModule::StartupModule()
{
	UE_LOG(Game, Warning, TEXT("Game module has started!"));
}
 
void FGameModule::ShutdownModule()
{
	UE_LOG(Game, Warning, TEXT("Game module has shut down"));
}
 
#undef LOCTEXT_NAMESPACE