// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModuleBootsrap.h"

#include "Engine/World.h"
#include "EnttIntegration/UnrealEnttSubsystem.h"
#include "EnttIntegration/Modules/ModuleInterface/ECSWorldSet.h"
#include "EnttIntegration/Modules/ModuleInterface/UEWorldSet.h"
#include "Modules/ModuleInterface/GameDataSet.h"

void AGameModuleBootsrap::Bootstrap()
{
	auto GameInstance = GetWorld()->GetGameInstance();
	auto ECS = GameInstance->GetSubsystem<UUnrealEnttSubsystem>();

	for(auto ModuleClass : EnttModules)
	{
		auto Module = NewObject<UEnttModuleBase>(this, ModuleClass);
		if(Module->Implements<UECSWorldSet>())
		{
			Cast<IECSWorldSet>(Module)->SetECS(ECS->GetECSWorld());
		}
		if(Module->Implements<UUEWorldSet>())
		{
			Cast<IUEWorldSet>(Module)->SetUEWorldSet(GetWorld());
		}
		if(Module->Implements<UGameDataSet>())
		{
			Cast<IGameDataSet>(Module)->SetGameData(GameData);
		}
		Module->Initialize();
	}
}
