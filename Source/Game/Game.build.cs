﻿using UnrealBuildTool;

public class Game : ModuleRules
{
	public Game(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.NoSharedPCHs;
		PrivatePCHHeaderFile = "PCHBasic.h";
		CppStandard = CppStandardVersion.Cpp17;

		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"YetAnotherSB",
			"UnrealEntt",
			"ProceduralMeshComponent"
		});
		PublicIncludePaths.AddRange(new string[] {"Game/Public"});
		PrivateIncludePaths.AddRange(new string[] {"Game/Private"});
	}
}